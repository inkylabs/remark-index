import { toText } from '@inkylabs/remark-utils'
import { visit } from 'unist-util-visit'

const NAME = 'index'

function verifyOverlap (root, f) {
  const starts = {}
  visit(root, ['indexstart', 'indexend'], (node, index, parent) => {
    const key = `${node.indexKey}.${node.groupKey}.${node.entryKey}`
    if (node.type === 'indexstart') {
      if (key in starts) {
        f.message(`index entry already in play: "${key}"`, node.position,
          `${NAME}:key-not-repeating`)
        return
      }
      starts[key] = node
      return
    }

    const start = starts[key]
    if (start) {
      delete starts[key]
      return
    }
    f.message(`index entry not in play: "${key}"`, node.position,
      `${NAME}:key-started`)
  })
  Object.entries(starts)
    .forEach(([key, node]) => {
      f.message(`index entry not ended: "${key}"`, node.position,
        `${NAME}:key-ended`)
    })
}

function verifyNames (root, f, names) {
  visit(root, ['indexstart', 'indexend'], (node, index, parent) => {
    let key = `${node.indexKey}.${node.groupKey}`
    if (key in names) {
      const n = names[key]
      if (n !== node.gn) {
        f.message(`inconsistent group name for "${key}"; ` +
          `was "${n}", is "${node.gn}"`, node.position, `${NAME}:matching-group-names`)
      }
    } else {
      names[key] = node.gn
    }

    key = `${key}.${node.entryKey}`
    if (key in names) {
      const n = names[key]
      if (n !== node.en) {
        f.message(`inconsistent entry name for key "${key}"; ` +
          `was "${n}", is "${node.en}"`, node.position, `${NAME}:matching-entry-names`)
      }
    } else {
      names[key] = node.en
    }
  })
}

// TODO: Move this out to its own package
//
// :x[]{}
// Attributes:
// - k = key to match node to.
// - ik = index key
// - ek = entry key
// - en = entry name
// - gk = group key
// - gn = group name
export default (opts) => {
  opts = Object.assign({
    key: 'X',
    data: {}
  }, opts)
  if (!opts.data.names) opts.data.names = {}
  if (!opts.data.nodes) opts.data.nodes = {}

  return (root, f) => {
    // Collect index formats.
    visit(root, 'leaf-indexfmt', (node, index, parent) => {
      const k = node.attributes.k || toText(node)
      if (k in opts.data.nodes) {
        f.message('repeat indexfmt key', node.position,
          `${NAME}:no-repeat-indexfmt-key`)
        return
      }
      opts.data.nodes[k] = {
        type: 'span',
        children: node.children,
        position: node.position
      }

      node.type = 'indexfmt'
      node.key = k
      node.indexKey = node.attributes.ik || opts.key
      opts.data.nodes[k] = node
    })

    // Get text of all nodes.
    visit(root, ['text-x', 'text-xi'], (node, index, parent) => {
      node.text = toText(node)
    })

    // Distinguish start and end nodes.
    visit(root, 'text-x', (node, index, parent) => {
      node.type = node.text ? 'text-x-start' : 'text-x-end'
      node.entries = []
      node.shortcut = node.text === '.'
    })

    // Parse topics.
    visit(root, ['text-x-start', 'text-xi'], (node, index, parent) => {
      node.topics = []
      if (node.shortcut) return
      node.topics = node.text.split(',')
        .map(t => t.replace('\n', ' ').trim())
        .map(t => [t, opts.data.nodes[t]])
    })

    // Populate shortcuts.
    let dotnode = null
    let types = ['text-x-start', 'text-x-end', 'text-xi']
    visit(root, types, (node, index, parent) => {
      switch (node.type) {
        case 'text-xi':
          node.type = 'span'
          if (!dotnode) {
            f.message('no shortcut index in play', node.position,
              `${NAME}:shortcut-started`)
            return
          }
          dotnode.topics.push(...node.topics)
          break
        case 'text-x-start':
          if (node.shortcut) dotnode = node
          break
        case 'text-x-end':
          dotnode = null
          break
      }
    })

    // Find start nodes without references.
    visit(root, 'text-x-start', (node, index, parent) => {
      if (node.topics.length) return
      f.message('Shortcut index not populated',
        node.position, `${NAME}:shortcut-populated`)
    })

    // Populate start entries.
    visit(root, 'text-x-start', (node, index, parent) => {
      node.entries.push(...node.topics
        .map(([t, n]) => [t, n, n ? n.attributes : {}])
        .map(([t, n, a]) => ({
          indexKey: node.attributes.ik || opts.key,
          entryKey: a.ok || t,
          entryName: n || {
            type: 'text',
            value: t,
            // TODO: Make this more precise.  We are currently using the same
            // position for each.
            position: node.position
          },
          groupKey: a.gk || node.attributes.gk,
          groupName: a.gn || node.attributes.gn
        })))
    })

    // Match start with end.
    let starts = {}
    visit(root, ['text-x-start', 'text-x-end'], (node, index, parent) => {
      const indexKey = node.attributes.ik || opts.key
      node.key = `${indexKey}:${node.attributes.k}`
      if (node.type === 'text-x-start') {
        if (starts[node.key]) {
          const line = starts[node.key].position.start.line
          f.message(`already have index started for ${node.key} on line ` +
              `${line}`, node.position, `${NAME}:no-double-starts`)
          node.type = 'index-error'
          return
        }
        node.type = 'indexgroupstart'
        starts[node.key] = node
        return
      }

      const start = starts[node.key]
      if (!start) {
        f.message(`cannot find start for ${node.key}`,
          node.position, `${NAME}:index-started`)
        return
      }
      node.type = 'indexgroupend'
      node.entries = start.entries
      delete starts[node.key]
    })

    // Find unended nodes.
    Object.values(starts)
      .forEach(s => {
        f.message(`cannot find end for ${s.key}`,
          s.position, `${NAME}:index-ended`)
        s.type = 'index-error'
      })

    // Match group starts to group ends.
    types = ['indexgroupstart', 'indexgroupend']
    starts = {}
    visit(root, types, (node, index, parent) => {
      if (node.type === 'indexgroupstart') {
        if (starts[node.key]) {
          const line = starts[node.key].position.start.line
          f.message(`indexgroup key "${node.key}" already in play on line ` +
              `${line}`,
          node.position, `${NAME}:group-not-repeating`)
        }
        const nodes = node.entries.map(e => Object.assign({
          type: 'indexstart',
          position: node.position
        }, e))
        parent.children.splice(index, 1, ...nodes)
        starts[node.key] = node
        return index
      }

      const start = starts[node.key]
      if (!start) {
        f.message(`index key "${node.key}" not in play`, node.position,
          `${NAME}:group-started`)
        return
      }
      const nodes = start.entries.map(e => Object.assign({
        type: 'indexend',
        position: node.position
      }, e))
      parent.children.splice(index, 1, ...nodes)
      delete starts[node.key]
      return index
    })

    // Find unended nodes.
    Object.values(starts)
      .forEach(s => {
        f.message(`cannot find end for ${s.key}`,
          s.position, `${NAME}:group-ended`)
      })

    verifyOverlap(root, f)
    verifyNames(root, f, opts.data.names)
  }
}
