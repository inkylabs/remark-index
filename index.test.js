/* eslint-env jest */
import { visit } from 'unist-util-visit'

import mod from '.'

const POS = {
  start: {
    line: 1
  }
}

function gennode (n) {
  const m = n.match(/^(.*?):(.*?)(?::(.*))?$/)
  switch (m[1]) {
    case 'x':
      return {
        type: 'text-x',
        children: [{
          type: 'text',
          value: m[2],
          position: POS
        }],
        attributes: {
          k: m[3]
        },
        position: POS
      }
    case 'xi':
      return {
        type: 'text-xi',
        children: [{
          type: 'text',
          value: m[2],
          position: POS
        }],
        attributes: {},
        position: POS
      }
    case 't':
      return {
        type: 'text',
        value: m[2],
        position: POS
      }
    case 'gs':
      return {
        type: 'indexgroupstart',
        key: m[2],
        entries: [],
        position: POS
      }
    case 'ge':
      return {
        type: 'indexgroupend',
        key: m[2],
        position: POS
      }
    case 'is':
      return {
        type: 'indexstart',
        entryKey: m[2],
        entryName: {
          type: 'text',
          value: m[2],
          position: POS
        },
        groupKey: undefined,
        groupName: undefined,
        indexKey: 'X',
        position: POS
      }
    case 'ie':
      return {
        type: 'indexend',
        entryKey: m[2],
        entryName: {
          type: 'text',
          value: m[2],
          position: POS
        },
        groupKey: undefined,
        groupName: undefined,
        indexKey: 'X',
        position: POS
      }
    case 's':
      return {
        type: 'span',
        children: [{
          type: 'text',
          value: m[2],
          position: POS
        }],
        attributes: {},
        position: POS
      }
  }
  throw new Error(`unknown node type ${m[1]}`)
}

function gendoc (spec) {
  return {
    type: 'root',
    children: spec
      .split(/ /g)
      .map(gennode)
  }
}

function cleandoc (root) {
  visit(root, 'span', (node, index, parent) => {
    delete node.text
    delete node.topics
  })
}

describe('xi', () => {
  const m = mod();
  [
    ['x:. xi:a x:', 'is:a s:a ie:a'],
    ['x:. xi:a xi:b x:', 'is:a is:b s:a s:b ie:a ie:b'],
    ['x:a,b x:', 'is:a is:b ie:a ie:b'],
    ['x:a:a x:b:b x::a x::b', 'is:a is:b ie:a ie:b']
  ]
    .forEach(([i, e]) => {
      it(i, () => {
        const f = { messages: [] }
        const d = gendoc(i)
        m(d, f)
        cleandoc(d)
        expect(f.messages).toEqual([])
        expect(d).toEqual(gendoc(e))
      })
    })
})

describe('messages', () => {
  const f = mod();
  [
    ['xi:a', 'shortcut-started'],
    ['x:. x:', 'shortcut-populated'],
    ['x:a x:a x:', 'no-double-starts'],
    ['x:', 'index-started'],
    ['x:. xi:a', 'index-ended'],
    ['gs: gs: ge:', 'group-not-repeating'],
    ['ge:', 'group-started'],
    ['gs:', 'group-ended'],
    ['is:a is:a ie:a', 'key-not-repeating'],
    ['ie:a', 'key-started'],
    ['is:a', 'key-ended']
  ]
    .forEach(([i, e]) => {
      it(`${i} ${e}`, () => {
        const d = { messages: [] }
        d.message = (x, y, z) => d.messages.push(z.replace(/.*:/, ''))
        const r = gendoc(i)
        f(r, d)
        expect(d.messages).toEqual([e])
      })
    })
})
